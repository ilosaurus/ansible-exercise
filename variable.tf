variable "total" {
 default = 4 
}


variable "hostname" {
  default = ["servera.lab.example.com","serverb.lab.example.com","serverc.lab.example.com","serverd.lab.example.com"] 
}


variable "address" {
  default = ["10.199.199.230","10.199.199.231","10.199.199.232","10.199.199.234"] 
}


variable "name" {
  default = "rhce-server0"
}

variable "image" {
  default = "CentOS-7-x86_64-GenericCloud-1907.qcow2"
}

variable "flavor" {
  default = "ns.2-2-20"
}

variable "keypair" {
  default = "ilham"
}

variable "network" {
  default = "0610bf20-6662-45db-ae6e-9e8bc5fe3f1e"
}
